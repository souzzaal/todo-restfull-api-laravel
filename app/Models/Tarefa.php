<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tarefa extends Model
{
    public $table = 'tarefas';
    public $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'conteudo',
        'id_usuario'
    ];

    public function scopeDoUsuario($query, $id_usuario)
    {
        return $query->where('id_usuario', $id_usuario);
    }
}
