<?php

namespace App\Http\Middleware;

use App\Http\Resources\ErroResource;
use Closure;
use Illuminate\Http\Response;
use JWTAuth;

class JwtAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(new ErroResource('Falha na autenticação', 'Token inválido', null), Response::HTTP_UNAUTHORIZED);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(new ErroResource('Falha na autenticação', 'Token expirado', null), Response::HTTP_UNAUTHORIZED);
            } else {
                return response()->json(new ErroResource('Falha na autenticação', 'Token não encontrado', null), Response::HTTP_UNAUTHORIZED);
            }
        }
        
        return $next($request);
    }
}
