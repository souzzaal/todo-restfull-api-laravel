<?php

namespace App\Http\Middleware;

use App\Http\Resources\ErroResource;
use Closure;
use Illuminate\Http\Response;

class JsonApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if ($request->header('content-type') != 'application/vnd.api+json') {
            return response()->json(['errors' => new ErroResource('Falha na validação', 'Tipo de conteúdo inválido')], Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
        }

        $response = $next($request);
       
        if ($response instanceof \Illuminate\Http\JsonResponse) {
           if (in_array($response->getStatusCode(), [200, 201])) {
                $jsonApiTopLevel = [
                    'data' => json_decode($response->getContent())
                ];
            } else {
                $jsonApiTopLevel = [
                    'errors' => json_decode($response->getContent())
                ];
            }

            $response->setData($jsonApiTopLevel);
        }

        $response->headers->set('Content-Type', 'application/vnd.api+json');

        return $response;
    }
}
