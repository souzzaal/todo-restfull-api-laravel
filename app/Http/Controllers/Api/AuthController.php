<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistroRequest;
use App\Http\Resources\ErroResource;
use App\Http\Resources\LoginResource;
use App\Models\Usuario;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{

    public function registro(Request $request)
    {
        $this->validateFields(new RegistroRequest());

        $inputs = $request->json('data.attributes');
        $usuario = Usuario::create($inputs);

        try {
            if (!$token = JWTAuth::fromUser($usuario)) {
                throw new Exception('Falha ao gerar token');
            }
            $usuario->token = $token;
            $usuario->time_expire = config('jwt.ttl');
        } catch (Exception $e) {
            return response()->json(
                new ErroResource('Erro', 'Não foi possível gerar a chave de acesso'),
                Response::HTTP_UNAUTHORIZED
            );
        }

        return response()->json(new LoginResource($usuario), Response::HTTP_OK);
    }

    public function login(Request $request)
    {
        $this->validateFields(new LoginRequest());

        $inputs = $request->json('data.attributes');

        $usuario = Usuario::where('email', $inputs['email'])
            ->first();

        if (!$usuario || !\Hash::check($inputs['senha'], $usuario->senha)) {
            return response()->json(
                new ErroResource('Credenciais inválidas', 'Usuário ou senha inválidos', ['pointer' => 'data.attributes.email'], 0),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        try {
            if (!$token = JWTAuth::fromUser($usuario)) {
                throw new Exception('Falha ao gerar token');
            }
            $usuario->token = $token;
            $usuario->time_expire = config('jwt.ttl');
        } catch (Exception $e) {
            return response()->json(
                new ErroResource('Erro', 'Não foi possível gerar a chave de acesso'),
                Response::HTTP_UNAUTHORIZED
            );
        }

        return response()->json(new LoginResource($usuario), Response::HTTP_OK);
    }

    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            return  response()->json([], Response::HTTP_OK);
        } catch (JWTException $exception) {
            return response()->json(
                new ErroResource('Erro', 'O usuário não está autenticado'),
                Response::HTTP_UNAUTHORIZED
            );
        }
    }
}
