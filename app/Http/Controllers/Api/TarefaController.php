<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TarefaCollection;
use App\Http\Requests\TarefaRequest;
use App\Http\Resources\ErroResource;
use App\Http\Resources\TarefaResource;
use App\Models\Tarefa;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TarefaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usuario = $request->user();

        $tarefas = Tarefa::doUsuario($usuario->id)->get();

        return response()->json(new TarefaCollection($tarefas));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateFields(new TarefaRequest());

        $inputs = $request->json('data.attributes');
        $inputs['id_usuario'] = $request->user()->id;

        $tarefa = Tarefa::create($inputs);

        return response()->json(new TarefaResource($tarefa));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = request()->user();
        $tarefa = Tarefa::find($id);

        if ($usuario->can('view', $tarefa)) {
            return response()->json(new TarefaResource($tarefa));
        } else {
            return response()->json(new ErroResource('Acesso negado', 'O usuário não tem permissão para visualizar esta tarefa', null, $id), Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateFields(new TarefaRequest());

        $tarefa = Tarefa::find($id);

        if ($request->user()->can('update', $tarefa)) {
            $inputs = $request->json('data.attributes');
            unset($inputs['id_usuario']);

            $tarefa->update($inputs);

            return response()->json(new TarefaResource($tarefa));
        } else {
            return response()->json(new ErroResource('Acesso negado', 'O usuário não tem permissão para editar esta tarefa', null, $id), Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tarefa = Tarefa::find($id);

        if (request()->user()->can('delete', $tarefa)) {

            $tarefa->delete();

            return response()->json([]);
        } else {
            return response()->json(new ErroResource('Acesso negado', 'O usuário não tem permissão para excluir esta tarefa', null, $id), Response::HTTP_FORBIDDEN);
        }
    }
}
