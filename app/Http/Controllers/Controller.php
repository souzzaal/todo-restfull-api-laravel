<?php

namespace App\Http\Controllers;

use App\Http\Resources\ErroCollection;
use App\Http\Resources\ErroResource;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use \Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     *  Realiza a validação dos dados enviados na requisição, 
     *  retornando mensagem de erro em caso de falha validação
     * 
     */
    public function validateFields(FormRequest $formRequest)
    {
        $fields = request()->json()->all();

        $validator = Validator::make($fields, $formRequest->rules(), $formRequest->messages());

        if($validator->fails()) {
            $errorMessages = json_decode($validator->messages()->toJson());

            $errors = collect();
            foreach ($errorMessages as $key => $error) {
                $errors->push(new ErroResource('Falha na validação', $error[0], ['pointer' => $key]));
            }

            throw new HttpResponseException(response()->json(new ErroCollection($errors), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }
}
