<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TarefaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = [
            'type' => 'tarefas',
                'id' => $this->id,
            'attributes' => [
                'titulo' => $this->titulo,
                'conteudo' => $this->conteudo
            ]
        ];

        return $resource;
    }
}
