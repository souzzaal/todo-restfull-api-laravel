<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = [
            'type' => 'login',
                'id' => $this->id,
            'attributes' => [
                'nome' => $this->nome,
                'email' => $this->email,
                'token' => $this->token,
                'time_expire' => $this->time_expire
            ]
        ];

        return $resource;
    }
}
