<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ErroResource extends JsonResource
{

    public function __construct($titulo, $detalhe, $origem = null, $id = null)
    {
        $this->titulo = $titulo;
        $this->detalhe = $detalhe;
        $this->origem = $origem;
        $this->id = $id;
    }
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = [
            'id' => $this->id,
            'title' => $this->titulo,
            'detail' => $this->detalhe,
            'source' => $this->origem
        ];
        
        return $resource;
    }
}
