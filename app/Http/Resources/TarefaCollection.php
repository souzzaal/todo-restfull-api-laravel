<?php

namespace App\Http\Resources;

use App\Models\Tarefa;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TarefaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Tarefa $tarefa){
            return new TarefaResource($tarefa);
        });

        return parent::toArray($request);
    }
}
