<?php

namespace App\Policies;

use App\Models\Usuario;
use App\Models\Tarefa;
use Illuminate\Auth\Access\HandlesAuthorization;

class TarefaPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any tarefas.
     *
     * @param  \App\Models\Usuario  $user
     * @return mixed
     */
    public function viewAny(Usuario $user)
    {
        
    }

    /**
     * Determine whether the user can view the tarefa.
     *
     * @param  \App\Models\Usuario  $user
     * @param  \App\Models\Tarefa  $tarefa
     * @return mixed
     */
    public function view(Usuario $user, Tarefa $tarefa)
    {
        return $user->id == $tarefa->id_usuario;
    }

    /**
     * Determine whether the user can create tarefas.
     *
     * @param  \App\Models\Usuario  $user
     * @return mixed
     */
    public function create(Usuario $user)
    {
        //
    }

    /**
     * Determine whether the user can update the tarefa.
     *
     * @param  \App\Models\Usuario  $user
     * @param  \App\Models\Tarefa  $tarefa
     * @return mixed
     */
    public function update(Usuario $user, Tarefa $tarefa)
    {
        return $user->id == $tarefa->id_usuario;
    }

    /**
     * Determine whether the user can delete the tarefa.
     *
     * @param  \App\Models\Usuario  $user
     * @param  \App\Models\Tarefa  $tarefa
     * @return mixed
     */
    public function delete(Usuario $user, Tarefa $tarefa)
    {
        return $user->id == $tarefa->id_usuario;
    }

    /**
     * Determine whether the user can restore the tarefa.
     *
     * @param  \App\Models\Usuario  $user
     * @param  \App\Models\Tarefa  $tarefa
     * @return mixed
     */
    public function restore(Usuario $user, Tarefa $tarefa)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the tarefa.
     *
     * @param  \App\Models\Usuario  $user
     * @param  \App\Models\Tarefa  $tarefa
     * @return mixed
     */
    public function forceDelete(Usuario $user, Tarefa $tarefa)
    {
        //
    }
}
