<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tarefa;
use App\Models\Usuario;
use Faker\Generator as Faker;

$factory->define(Tarefa::class, function (Faker $faker) {
    return [
        'titulo' => $faker->word,
        'conteudo' => $faker->paragraph,
        'id_usuario' => factory(Usuario::class)->create()->id
    ];
});
