<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('test', function() {
    return response()->json('{method: "'.request()->method().'", status: "OK"}');
});

Route::namespace('Api')->group(function() {

    Route::post('login', 'AuthController@login');
    Route::post('registro', 'AuthController@registro');
    Route::group(['middleware' => ['auth.jwt']], function() {
        Route::post('logout', 'AuthController@logout');
        Route::resource('tarefas', 'TarefaController');
    });    

});