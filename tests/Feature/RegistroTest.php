<?php

namespace Tests\Feature;

use App\Models\Usuario;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;

class RegistroTest extends TestCase
{
    
    use DatabaseTransactions;

    public function testRegistro()
    {
        $usuario = factory(Usuario::class)->make();        

        $json = '{
            "data": {
                "type": "registro",
                "attributes": {
                    "nome": "'.$usuario->nome.'",
                    "email":"'.$usuario->email.'",
                    "senha":"1234"
                }
            }
        }';
        
        $response = $this->json('POST', 'api/registro', json_decode($json, true), [
                'content-type' => 'application/vnd.api+json'
            ]);
        
        $response->assertStatus(Response::HTTP_OK);
        
        $data = $response->getData()->data;

        $this->assertEquals('login', $data->type);
        $this->assertObjectHasAttribute('id', $data);
        $this->assertObjectHasAttribute('email', $data->attributes);
        $this->assertObjectHasAttribute('token', $data->attributes);
        $this->assertObjectHasAttribute('time_expire', $data->attributes);
    }

    /**
     * @dataProvider dadosInvalidosProvider
     */
    public function testRegistroComDadosInvalidos($type, $nome, $email, $senha)
    {
        $json = '{
            "data": {
                "type": "'.$type.'",
                "attributes": {
                    "nome": "'.$nome.'",
                    "email":"'.$email.'",
                    "senha":"'.$senha.'"
                }
            }
        }';
        
        $response = $this->json('POST', 'api/registro', json_decode($json, true), [
                'content-type' => 'application/vnd.api+json'
            ]);
        
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    
    public function dadosInvalidosProvider()
    {
        $this->createApplication();
        $type = 'registro';
        $usuario = factory(Usuario::class)->make();

        return [
            'sem dados' => ['', '', '', ''],
            'sem tipo' => ['', $usuario->nome, $usuario->email, $usuario->senha],
            'sem nome' => [$type, '', $usuario->email, $usuario->senha],
            'sem email' => [$type, $usuario->nome, '', $usuario->senha],
            'com email invalido' => [$type, $usuario->nome, 'user.com', $usuario->senha],
            'sem senha' => [$type, $usuario->nome, $usuario->email, '']
        ];
    }

}
