<?php

namespace Tests\Feature;

use App\Models\Tarefa;
use App\Models\Usuario;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use JWTAuth;

class TarefaTest extends TestCase
{
    use DatabaseTransactions;

    public function testListagemNaoAutenticado()
    {
        $response = $this->json('GET','/api/tarefas', [], [
            'content-type' => 'application/vnd.api+json'
        ]);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testListagem()
    {
        $usuario = factory(Usuario::class)->create();
        $tarefasDeOutrosUsuarios = factory(Tarefa::class, 5)->create();
        $tarefas = factory(Tarefa::class, 5)->create([
            'id_usuario' => $usuario->id
        ]);

        $response = $this->json('GET','/api/tarefas', [], [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);

        $response->assertStatus(Response::HTTP_OK);

        $data = $response->getData()->data;
        
        foreach($data as $tarefa) {
            $this->assertEquals('tarefas', $tarefa->type);
            $this->assertObjectHasAttribute('id', $tarefa);
            $this->assertObjectHasAttribute('titulo', $tarefa->attributes);
            $this->assertObjectHasAttribute('conteudo', $tarefa->attributes);
            $this->assertEquals($usuario->id, Tarefa::find($tarefa->id)->id_usuario);
        }
    }

    public function testCadastraTarefa()
    {
        $usuario = factory(Usuario::class)->create();
        $tarefa = factory(Tarefa::class)->make();

        $json = '{
            "data": {
                "type": "tarefas",
                "attributes": {
                    "titulo": "'.$tarefa->titulo.'",
                    "conteudo":"'.$tarefa->conteudo.'"
                }
            }
        }';

        $response = $this->json('POST','/api/tarefas', json_decode($json, true), [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);

        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @dataProvider dadosInvalidosProvider
     */
    public function testCadastraTarefaComDadosInvalidos($type, $titulo, $conteudo)
    {
        $usuario = factory(Usuario::class)->create();

        $json = '{
            "data": {
                "type": "'.$type.'",
                "attributes": {
                    "titulo": "'.$titulo.'",
                    "conteudo":"'.$conteudo.'"
                }
            }
        }';

        $response = $this->json('POST','/api/tarefas', json_decode($json, true), [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testBuscaTarefaDoProprioUsuario()
    {
        $usuario = factory(Usuario::class)->create();
        $tarefa = factory(Tarefa::class)->create([
            'id_usuario' => $usuario->id
        ]);

        $response = $this->json('GET','/api/tarefas/'.$tarefa->id, [], [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);
        
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testBuscaTarefaDeOutroUsuario()
    {
        $usuario = factory(Usuario::class)->create();
        $tarefa = factory(Tarefa::class)->create();

        $response = $this->json('GET','/api/tarefas/'.$tarefa->id, [], [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);
        
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testBuscaTarefaInexistente()
    {
        $usuario = factory(Usuario::class)->create();

        $response = $this->json('GET','/api/tarefas/0', [], [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);
        
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testEditaTarefaDoProprioUsuario()
    {
        $usuario = factory(Usuario::class)->create();
        $tarefa = factory(Tarefa::class)->create([
            'id_usuario' => $usuario->id
        ]);

        $json = '{
            "data": {
                "id": "'.$tarefa->id.'",
                "type": "tarefas",
                "attributes": {
                    "titulo": "titulo da tarefa",
                    "conteudo":"conteudo da tarefa"
                }
            }
        }';

        $response = $this->json('PUT','/api/tarefas/'.$tarefa->id, json_decode($json, true), [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);

        $response->assertStatus(Response::HTTP_OK);

        $data = $response->getData()->data;
        
        $this->assertEquals($tarefa->id, $data->id);
        $this->assertEquals('titulo da tarefa', $data->attributes->titulo);
        $this->assertEquals('conteudo da tarefa', $data->attributes->conteudo);
    }

    public function testEditaTarefaInexistente()
    {
        $usuario = factory(Usuario::class)->create();

        $json = '{
            "data": {
                "id": "0",
                "type": "tarefas",
                "attributes": {
                    "titulo": "titulo da tarefa",
                    "conteudo":"conteudo da tarefa"
                }
            }
        }';

        $response = $this->json('PUT','/api/tarefas/0', json_decode($json, true), [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testEditaTarefaDeOutroUsuario()
    {
        $usuario = factory(Usuario::class)->create();
        $tarefa = factory(Tarefa::class)->create();

        $json = '{
            "data": {
                "id": "'.$tarefa->id.'",
                "type": "tarefas",
                "attributes": {
                    "titulo": "titulo da tarefa",
                    "conteudo":"conteudo da tarefa"
                }
            }
        }';

        $response = $this->json('PUT','/api/tarefas/'.$tarefa->id, json_decode($json, true), [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * @dataProvider dadosInvalidosProvider
     */
    public function testEditaTarefaComDadosInvalidos($type, $titulo, $conteudo)
    {
        $usuario = factory(Usuario::class)->create();
        $tarefa = factory(Tarefa::class)->create();

        $json = '{
            "data": {
                "id": "'.$tarefa->id.'",
                "type": "'.$type.'",
                "attributes": {
                    "titulo": "'.$titulo.'",
                    "conteudo":"'.$conteudo.'"
                }
            }
        }';

        $response = $this->json('PUT','/api/tarefas/'.$tarefa->id, json_decode($json, true), [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testExcluiTarefaDoProprioUsuario()
    {
        $usuario = factory(Usuario::class)->create();
        $tarefa = factory(Tarefa::class)->create([
            'id_usuario' => $usuario->id
        ]);

        $response = $this->json('DELETE','/api/tarefas/'.$tarefa->id, [], [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('tarefas', ['id' => $tarefa->id]);
    }

    public function testExcluiTarefaDeOutroUsuario()
    {
        $usuario = factory(Usuario::class)->create();
        $tarefa = factory(Tarefa::class)->create();

        $response = $this->json('DELETE','/api/tarefas/'.$tarefa->id, [], [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('tarefas', ['id' => $tarefa->id]);
    }

    public function testExcluiTarefaInexistente()
    {
        $usuario = factory(Usuario::class)->create();

        $response = $this->json('DELETE','/api/tarefas/0', [], [
            'content-type' => 'application/vnd.api+json',
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($usuario)
        ]);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function dadosInvalidosProvider()
    {
        $type = 'tarefas';
        $titulo = 'titulo';
        $conteudo = 'conteudo';

        return [
            'sem dados' => ['', '', ''],
            'sem type' => ['', $titulo, $conteudo],
            'type invalido' => ['asdf', $titulo, $conteudo],
            'sem titulo' => [$type, '', $conteudo],
            'sem conteudo' => [$type, $titulo, '']
        ];
    }


}
