<?php

namespace Tests\Feature;

use App\Models\Usuario;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use JWTAuth;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    public function testRequisicaoUsandoTipoDeConteudoInvalido() 
    {
        $response = $this
            ->json('POST', 'api/logout', []);

        $response->assertStatus(Response::HTTP_UNSUPPORTED_MEDIA_TYPE);

        $data = $response->getData();
        $this->assertObjectHasAttribute('errors', $data);
        $this->assertObjectHasAttribute('id', $data->errors);
        $this->assertObjectHasAttribute('title', $data->errors);
        $this->assertObjectHasAttribute('detail', $data->errors);
        $this->assertObjectHasAttribute('source', $data->errors);
    }

    public function testAcessandoRotaFechadaSemToken() 
    {
        $response = $this
            ->json('POST', 'api/logout', [], [
                'content-type' => 'application/vnd.api+json'
            ]);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);

        $data = $response->getData();
        
        $this->assertObjectHasAttribute('errors', $data);
        $this->assertObjectHasAttribute('id', $data->errors);
        $this->assertObjectHasAttribute('title', $data->errors);
        $this->assertObjectHasAttribute('detail', $data->errors);
        $this->assertObjectHasAttribute('source', $data->errors);
    }
    
    public function testAcessandoRotaFechadaComTokenInvalido() 
    {                
        $response = $this
            ->json('POST', 'api/logout', [], [
                'Authorization' => 'Bearer asdf1234',
                'content-type' => 'application/vnd.api+json'
            ]);
        
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);        
    }

    /**
     * @dataProvider dadosInvalidosProvider
     */
    public function testLoginComDadosInvalidos($type, $email, $senha)
    {
        $json = '{
            "data": {
                "type": "'.$type.'",
                "attributes": {
                    "email":"'.$email.'",
                    "senha":"'.$senha.'"
                }
            }
        }';
        
        $response = $this->json('POST', 'api/login', json_decode($json, true), [
                'content-type' => 'application/vnd.api+json'
            ]);
        
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testLogin()
    {

        $usuario = factory(Usuario::class)->create();        

        $json = '{
            "data": {
                "type": "login",
                "attributes": {
                    "email":"'.$usuario->email.'",
                    "senha":"1234"
                }
            }
        }';
        
        $response = $this->json('POST', 'api/login', json_decode($json, true), [
                'content-type' => 'application/vnd.api+json'
            ]);
        
        $response->assertStatus(Response::HTTP_OK);
        
        $data = $response->getData()->data;

        $this->assertEquals('login', $data->type);
        $this->assertObjectHasAttribute('id', $data);
        $this->assertObjectHasAttribute('email', $data->attributes);
        $this->assertObjectHasAttribute('token', $data->attributes);
        $this->assertObjectHasAttribute('time_expire', $data->attributes);
    }

    public function testLogout()
    {
         $user = factory(Usuario::class)->create();
         $token = JWTAuth::fromUser($user);
         
         $response = $this->json('POST', 'api/logout', [], [
            'Authorization' => 'Bearer ' . $token,
            'content-type' => 'application/vnd.api+json'
        ]);
        
        $response->assertStatus(Response::HTTP_OK);
    }

    public function dadosInvalidosProvider()
    {
        $type = 'login';
        $email = 'user@mail.com';
        $senha = 'asdf';

        return [
            'sem dados' => ['', '', ''],
            'sem type' => ['', $email, $senha],
            'sem email' =>  [$type, '', $senha],
            'sem senha' => [$type, $email, '']
        ];
    }
}
